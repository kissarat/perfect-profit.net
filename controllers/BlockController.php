<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;
use app\behaviors\Access;
use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Block;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class BlockController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['index', 'edit', 'delete'],
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Block::find(),
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_DESC
                    ]
                ]
            ])
        ]);
    }

    public function actionEdit($ip = null) {
        $model = null;
        if ($ip) {
            $model = Block::findOne($ip);
        }
        if (!$model) {
            $model = new Block(['ip' => $ip]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete($ip) {
        $model = Block::findOne($ip);
        if ($model && $model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Success'));
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Fail'));
        }

        return $this->redirect(['index']);
    }
}
