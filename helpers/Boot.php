<?php

namespace app\helpers;


use yii\base\Application;
use yii\base\BootstrapInterface;

class Boot implements BootstrapInterface {

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function($event) {
            throw new Exception();
            if (Block::isBlocked($_SERVER['REMOTE_ADDR'])) {
                Yii::$app->response->redirect('/block.php');
            }
        });
    }
}
