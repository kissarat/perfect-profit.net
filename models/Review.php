<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;


use app\behaviors\Journal;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Review
 * @property integer $id
 * @property string $user_name
 * @property string $content
 * @package app\models
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Review extends ActiveRecord {

    public function behaviors() {
        return [
            Journal::className()
        ];
    }

    public function rules() {
        return [
            ['content', 'required'],
            ['content', 'string', 'min' => 20, 'max' => 4000],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['content'],
            'manage' => ['user_name', 'content']
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['name' => 'user_name']);
    }

    public function attributeLabels() {
        return [
            'user_name' => Yii::t('app', 'Username'),
            'content' => Yii::t('app', 'Content')
        ];
    }

    public function __toString() {
        return substr($this->content, 0, 60);
    }
}
