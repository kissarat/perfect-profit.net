<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\article\controllers;

use app\behaviors\Access;
use app\models\search\Record;
use app\modules\article\models\Article;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * PostController implements the CRUD actions for Post model.
 */
class ArticleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => Access::class,
                'manager' => ['create', 'update', 'delete', 'pages']
            ],

            'cache' => [
                'class' => 'yii\filters\HttpCache',
                'cacheControlHeader' => 'must-revalidate, private',
                'only' => ['index', 'page', 'view'],
                'enabled' => 'main' == Yii::$app->layout,
                'lastModified' => function ($action, $params) {
                    $query = Record::find()->andWhere("type = 'article'");
                    switch ($action->id) {
                        case 'view':
                            $query->andWhere(['object_id' => $_GET['id']]);
                            break;
                        case 'page':
                            $query
                                ->innerJoin('article', 'object_id = article.id')
                                ->andWhere(['name' => $_GET['name']]);
                            break;
                    }

                    return strtotime($query->max('time'));
                },
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Article::find()->where('"name" is null')->orderBy(['id' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 14
                ],
            ])
        ]);
    }

    public function actionPages()
    {
        return $this->render('pages', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Article::find()->where('"name" is not null')->orderBy(['id' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 14
                ],
            ])
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->name) {
            return $this->redirect(['page', 'name' => $model->name]);
        }
        else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionPage($name)
    {
        return $this->render('view', [
            'model' => Article::findOne(['name' => $name]),
        ]);
    }

    public function actionCreate($scenario = 'default')
    {
        $model = new Article(['scenario' => $scenario]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->name) {
            $model->scenario = 'page';
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));
        }
    }
}
