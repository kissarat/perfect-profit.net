<?php
/**
 * @link http://zenothing.com/
*/

namespace app\modules\bank\models;

use app\models\Message;
use app\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "income".
 *
 * @property integer $id
 * @property integer $node_id
 * @property string $user_name
 * @property number $amount
 * @property integer $time
 *
 * @property Node $node
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Income extends ActiveRecord
{
    public static function tableName() {
        return 'income';
    }

    public function rules() {
        return [
            [['node_id', 'user_name', 'time'], 'required'],
            [['node_id', 'time'], 'integer'],
            [['user_name'], 'string', 'max' => 24]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'node_id' => Yii::t('app', 'Investment ID'),
            'user_name' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getNode() {
        return $this->hasOne(Node::class, ['id' => 'node_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public static function make(Node $node) {
        $income = new Income([
            'node_id' => $node->id,
            'user_name' => $node->user_name,
            'amount' => $node->getIncome(),
            'time' => $node->time
        ]);
        if ($income->save(false)) {
            Message::send($node->user_name, 'info', 'You got income');
            return true;
        }
        return false;
    }
}
