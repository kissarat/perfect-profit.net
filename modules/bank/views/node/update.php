<?php
/**
 * @link http://zenothing.com/
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\bank\models\Node */

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['invest', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="node-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
