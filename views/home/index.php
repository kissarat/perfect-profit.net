<?php
/**
 * @link http://zenothing.com/
 * @var string $statistics
 */

use app\modules\article\models\Article;
use app\widgets\Ext;

$this->title = Yii::$app->name;
?>
<div class="home-index">
    <?= Ext::stamp() ?>

    <div class="paperbg">
        <?= Yii::$app->view->renderFile('@app/modules/article/views/article/view.php', [
            'model' => Article::findOne(['name' => 'marketing'])
        ]) ?>
    </div>
</div>
