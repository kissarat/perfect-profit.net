<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\MainAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/img/cover.png" />
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="<?= Yii::$app->controller->id . ' ' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>
<header>
    <table>
        <tr>
            <td>
                <div class="logo"><img src="images/logo.png"> </div>
            </td>
            <td>
                <?php if (Yii::$app->user->isGuest): ?>
                <table id="login">
                    <tr>
                        <td>
                            <b>Логин:</b>
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control"  aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Пароль:</b>
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control"  aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="checkbox"/><span>Запомнить меня</span></td>
                    </tr>
                    <tr>
                        <td colspan="2"><a href="#">Регистрация </a><span> | </span><a href="#"> Забыли пароль? </a><a href="#" class="btn btn-default" role="button">Войти</a></p> </td>
                    </tr>
                </table>
                <?php endif; ?>
            </td>
        </tr>
    </table>


<!-- <div class="headcont">

    <?php if (Yii::$app->user->isGuest): ?>
        <div class="gbuttons">
            <div class="login button">
                <?= Html::a(Yii::t('app', 'Login'), ['user/login'], ['class' => 'signup']) ?>
            </div>

            <div class="login button">
                <?= Html::a(Yii::t('app', 'Signup'), ['user/signup'], ['class' => 'signup']) ?>
            </div>
        </div>
    <?php endif; ?>
</div> -->

<div class="wrap <?= $login ?>">
    <ol class="breadcrumb">
    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
        ['label' => Yii::t('app', 'About'), 'url' => ['/article/article/page', 'name' => 'about']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/create']],
        ['label' => Yii::t('app', 'Reviews'), 'url' => ['/review/index']]
    ];

    if (Yii::$app->user->isGuest) {
    }
    else {
        $items[] = ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/view']];
        $items[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout']];
        if ($manager) {
            $items[0] = ['label' => Yii::t('app', 'Admin Panel'),
                'url' => 'http://admin.' . $_SERVER['HTTP_HOST'],
                'options' => [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'auth' => Yii::$app->user->identity->auth
                        ]
                    ]
                ]
            ];
        }
    }

    if (Yii::$app->user->isGuest || !Yii::$app->user->identity->isManager()) {
        $items[] = 'ru' == Yii::$app->language
            ? ['label' => 'EN', 'url' => ['/lang/lang/choice', 'code' => 'en'], 'options' => ['title' => 'English']]
            : ['label' => 'RU', 'url' => ['/lang/lang/choice', 'code' => 'ru'], 'options' => ['title' => 'Русский']];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>
        </ol>
</header>

    <div class="container">
        <div id="intro">

        </div>
        <div class="main">
            <div id="question">
                <div class="voprostop"></div>
                <table>
                    <tr>
                        <h1>Задать вопрос?</h1>
                    </tr>
                    <hr>
                    <tr>
                        <p>Ваше имя</p>
                    </tr>
                    <tr>
                        <div class="input-group">
                            <input type="text" class="form-control"  aria-describedby="basic-addon1">
                        </div>
                    </tr>
                    <tr>
                        <p>E-mail</p>
                    </tr>
                    <tr>
                        <div class="input-group">
                            <input type="text" class="form-control"  aria-describedby="basic-addon1">
                        </div>
                    </tr>
                    <tr>
                        <p>Вопрос</p>
                    </tr>
                    <tr>
                        <textarea></textarea>
                    </tr>
                    <tr>
                        <a href="#" class="btn btn-default" role="button">Отправить</a>
                    </tr>
                </table>
            </div>
            <div id="main-text">
                <table>
                    <tr><h1>Углавление</h1></tr>
                    <hr>
                    <tr>
                        <th><p>Текст текст Текст текст Текст текст Текст текст
                                текст текст текст текст текст текст текст текст текст
                                текст текст текст текст текст текст текст текст текст текст текст текст текст текст
                                текст текст текст текст текст текст текст текст текст текст текст текст
                                текст текст текст текст текст текст текст текст текст текст текст текст
                            </p></th>
                        <td rowspan="2"></td>
                    </tr>

                    <tr><th><p>текст текст текст текст текст текст текст текст текст
                                текст текст текст текст текст текст текст текст текст
                                текст </p></th></tr>
                </table>
            </div>
        </div>
        <div class="text">
            <div class="voprostop"></div>
            <table>
                <tr>

                        <?= Breadcrumbs::widget([
                            'homeLink' => false,
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= Alert::widget() ?>
                        <?php
                        ?>
                        <?= $content ?>

                </tr>
            </table>
        </div>
    </div>
</div>

<div class="background"></div>
<div id="linux">
    <div>
        <img src="/images/linux.png" />
    </div>
    <?= Html::tag('div', Yii::t('app', 'Welcome, Linux user. We are glad you use open source software!'), [
        'class' => 'welcome'
    ]) ?>
    <div class="glyphicon glyphicon-remove"></div>
</div>
<div class="foot">
    <ol class="breadcrumb">
        <li ><a href="index.html">Главная</a></li>
        <li><a href="about.html">О НАС</a></li>
        <li><a href="marketing.html">Маркетинг</a></li>
        <li><a href="#">FAQ</a></li>
    </ol>
    <div id="metrika">
        <!-- Yandex.Metrika informer -->
        <a href="https://metrika.yandex.ru/stat/?id=32216169&amp;from=informer"
           target="_blank" rel="nofollow">
            <img src="https://informer.yandex.ru/informer/32216169/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
                 alt="Яндекс.Метрика"
                 title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" />
        </a>
</div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
