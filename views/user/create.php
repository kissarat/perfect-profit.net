<?php
/**
 * @link http://zenothing.com/
*/

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::$app->user->isGuest ? Yii::t('app', 'Signup') : Yii::t('app', 'Create User');
?>
<div class="regpic"></div>
<div class="user-create">
    <div class="regbg">
    <table>
        <tr>
            <td class="regtc"></td>
            <td class="regf">
    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
            </td>
            <td class="regtr"></td>
        </tr>
    </table>
        </div>
</div>
