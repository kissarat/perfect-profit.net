<?php
/**
 * @link http://zenothing.com/
*/

use app\widgets\Ext;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Login */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Login');
if ('admin' == Yii::$app->layout) {
    $this->title = Yii::t('app', 'Admin Panel') . ': ' . $this->title;
}
?>
<div class="user-login">
    <div class="regbg">
    <?= Ext::stamp() ?>
    <h1 class="bagatelle"><?= $this->title ?></h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'remember')->checkbox() ?>

    <?php if ('main' == Yii::$app->layout): ?>
    <div class="form-group">
        <?= Yii::t('app', 'You can recover <a href="{url}">your password</a>', [
            'url' => Url::to(['request']),
        ]); ?>
    </div>
    <?php endif ?>

    <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
        </div>
</div><!-- user-login -->
