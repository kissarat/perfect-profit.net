<?php
/**
 * @link http://zenothing.com/
 */
define('CONFIG', __DIR__ . '/../config');

use yii\web\Application;

require_once CONFIG . '/boot.php';
require_once CONFIG . '/web.php';

$app = new Application($config);
$app->run();